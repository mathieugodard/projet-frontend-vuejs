import axios from 'axios'

axios.defaults.baseURL = 'http://127.0.0.1:8000/'

export const state = () => ({
  token: '',
  user: '',
})

axios.defaults.headers.common.Authorization = state.token

export const mutations = {
  login(state, { token, user }) {
    state.token = token
    state.user = user
    // // eslint-disable-next-line no-console
    // console.log('user et token dans le store : ', state.user, state.token)
  },
  logout(state) {
    state.token = ''
    state.user = ''
  },
  setUser(state, user) {
    state.user = user
  },
}

export const getters = {
  isAuth(state) {
    return state.user !== ''
  },
}
